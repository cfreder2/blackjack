package blackjack;

import java.util.Scanner;

public class GameService {
    private Human human;
    private Dealer dealer;

    public GameService() {
	human = new Human();
	dealer = new Dealer(new Hand(), new Hand(), new Deck());

    }

    public void startGame() {

	GameStatus status;

	Scanner scan = new Scanner(System.in);
	System.out.println("\nWelcome to blackjack. New game will now begin.");
	dealer.shuffle();
	dealer.dealHand();

	status = GameStatus.PLAYERTURN;
	printRelevantHandInfo(status);
	status = checkInitialGameStatus(dealer);

	while (status == GameStatus.PLAYERTURN) {
	    status = checkGameStatus(human.decide(dealer), dealer);
	    printRelevantHandInfo(status);
	}
	
	while (status == GameStatus.DEALERTURN) {
	    status = checkGameStatus(dealer.decide(dealer), dealer);
	    printRelevantHandInfo(status);
	    
	}

	boolean validInput = false;
	while (!validInput) {
	    System.out.println("\nWould you like to play again (p) or quit (q)?");

	    String decision = scan.nextLine();

	    if (decision.equals("p")) {
		validInput = true;
		dealer.resetGame();
		startGame();

	    } else if (decision.equals("q")) {
		System.exit(0);
		validInput = true;
	    } else {
		System.out.println("\nSorry, that was not an option");
	    }
	}

    }

    public GameStatus checkInitialGameStatus(Dealer d) {
	if (d.getDealerHand().getValue() == 21) {
	    if (d.getPlayerHand().getValue() == 21) {
		
		System.out.println("\nIt's a tie!");
		return GameStatus.TIE;

	    } else {
		System.out.println("\nDealer gets a blackjack! You lose.");
		return GameStatus.DEALERWON;
	    }
	} else if (d.getPlayerHand().getValue() == 21) {
	    System.out.println("\nBlackjack! You win");
	    return GameStatus.PLAYERWON;
	}
	return GameStatus.PLAYERTURN;

    }

    public GameStatus checkGameStatus(GameStatus status, Dealer d) {

	if (status == GameStatus.PLAYERTURN || status == GameStatus.PLAYERFINISHED) {
        	if (d.getPlayerHand().getValue() > 21) {
        	    System.out.println("\nYou busted. Good game, well played");
        	    return GameStatus.DEALERWON;
        	}
        	else if (status == GameStatus.PLAYERFINISHED) {
        	    return GameStatus.DEALERTURN;
        	}
	}
	else if (status == GameStatus.GAMEENDED){
        	if (d.getDealerHand().getValue() > 21) {
        	    System.out.println("\nDealer busted. Well played!.");
        	    return GameStatus.PLAYERWON;
        	} 
        	else {
        	    if (d.getPlayerHand().getValue() > d.getDealerHand().getValue()) {
        		System.out.println("\nYou win!");
        		return GameStatus.PLAYERWON;
        	    } else if (d.getPlayerHand().getValue() == d.getDealerHand()
        		    .getValue()) {
        		System.out.println("\nIt's a tie.");
        		return GameStatus.TIE;
        	    } else {
        		System.out.println("\nYou lose.");
        		return GameStatus.DEALERWON;
        	    }
        	}
	}

	return status;
    }
    
    public void printRelevantHandInfo(GameStatus status) {
	if (status == GameStatus.DEALERTURN) {
	    System.out.println("\nDealer's hand: " + dealer.getDealerHand().toString());
	}
	else {
	    System.out.println("\nYour hand: " + dealer.getPlayerHand().toString());
	    System.out.println("Dealer's hand: " + dealer.getDealerHand().toString());
	}
    }
    
}
