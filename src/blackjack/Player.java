package blackjack;


public interface Player
{
    public GameStatus decide(Dealer dealer);
    
}
