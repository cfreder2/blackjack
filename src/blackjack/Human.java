package blackjack;
import java.util.Scanner;


public class Human implements Player
{

    public Human()
    {

    }

    @Override
    public GameStatus decide(Dealer dealer)
    {
	Scanner scan = new Scanner(System.in);
	
	System.out.println("Your turn.");
	
	boolean validInput = false;
	while (!validInput) {
    	    System.out.println("Would you like to hit (h) or stand (s)?");
    	    
    	    String decision = scan.nextLine();
    	    
    	    if (decision.equals("h")) {

    		validInput = true;
    		hit(dealer);
    	    }
    	    else if (decision.equals("s")) {
    		validInput = true;
    		stand();
    		return GameStatus.PLAYERFINISHED;
    	    }
    	    else {
    		System.out.println("You can't do that in blackjack");
    	    }
		
	}
	return GameStatus.PLAYERTURN;
    }
    
    private void hit(Dealer dealer) {
	dealer.dealCard(GameStatus.PLAYERTURN);
	System.out.println("You hit.");
    }
    
    private void stand() {
	System.out.println("You stand.");

    }

}
