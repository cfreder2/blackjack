package blackjack;

public enum GameStatus
{
    PLAYERTURN,
    PLAYERFINISHED,
    DEALERTURN,
    GAMEENDED,
    TIE,
    PLAYERWON,
    DEALERWON
}
