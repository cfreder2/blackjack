package blackjack;

public class Card
{
    
	public static enum Value {
	    TWO(2), 
	    THREE(3), 
	    FOUR(4), 
	    FIVE(5), 
	    SIX(6), 
	    SEVEN(7),
	    EIGHT(8), 
	    NINE(9), 
	    TEN(10), 
	    JACK(10), 
	    KING(10), 
	    QUEEN(10),
	    ACE(11);
	
	    private int val;
	    
	    Value(int val) {
		this.val = val;
	    }
	
	    public int getVal() {
		return this.val;
	    }
	}
	
	public static enum Suit {
	    SPADES, 
	    HEARTS, 
	    CLUBS,
	    DIAMONDS
	

	}
	
    private final Value val;
    private final Suit suit;
    

    public Card(Value val, Suit suit)
    {
	this.val = val;
	this.suit = suit;
	// TODO Auto-generated constructor stub
    }
    
    public int getVal() {
	return val.getVal();
    }
    
    public Suit getSuit() {
	return suit;
    }

    public String toString() {
	return val.toString() + " of " + suit.toString();
    }
    
    public boolean equals(Object c) {
	if ( this == c ) return true;
	
	if (!(c instanceof Card)) return false;
	
	Card card = (Card) c;
	
	if (card.suit == this.suit && card.val == this.val) return true;
	return false;
    }
}
