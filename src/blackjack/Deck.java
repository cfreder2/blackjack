package blackjack;
import java.util.ArrayList;


public class Deck
{
    private ArrayList<Card> deck;


    public Deck()
    {
	initializeDeck();
    }
    
    public void initializeDeck() {
	if (deck == null) {
	    deck = new ArrayList<Card>();
	}
	else {
	    deck.clear();
	}
	for(Card.Value val: Card.Value.values()) {
	    for(Card.Suit suit: Card.Suit.values()) {
		deck.add(new Card(val, suit));
	    }
	}
    }
    
    public void shuffle() {
	ArrayList<Card> shuffledDeck = new ArrayList<Card>();
	while (!deck.isEmpty()) {
	    shuffledDeck.add(
		    deck.remove(
			    (int)(Math.random()*deck.size())));
	}
	deck = shuffledDeck;
    }
    
    public Card drawCard() {
	return deck.remove(0);
    }
    
    public ArrayList<Card> getDeck() {
	return deck;
    }
    
    public String toString() {
	return deck.toString();
    }
    
    public boolean equals(Object d) {
	if ( this == d ) return true;
	
	if (!(d instanceof Deck)) return false;
	
	Deck deck2 = (Deck) d;
	ArrayList<Card> cards2 = deck2.getDeck();
	for (int i = 0; i > cards2.size(); i++)
	    if (!deck.get(i).equals(cards2.get(i))) return false;
	return true;
    }
}
