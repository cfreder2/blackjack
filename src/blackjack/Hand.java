package blackjack;
import java.util.ArrayList;


public class Hand
{
    private ArrayList<Card> hand;
    private int aceCount;

    public Hand()
    {
	hand = new ArrayList<Card>();
	aceCount = 0;
    }
    
    public Hand(ArrayList<Card> hand, int aceCount) {
	this.hand = hand;
	this.aceCount = aceCount;
    }
    
    public void drawCard(Card card) {
	if (card.getVal() == 11) {
	    aceCount ++;
	}
	hand.add(card);
    }

    public int getValue() {
	int val = 0;
	
	for (int i = 0; i < hand.size(); i++) {
	    int cardVal = hand.get(i).getVal();

	    val += cardVal;
	}
	
	if (val > 21) {
	    while (aceCount > 0 && val > 21) {
		aceCount--;
		val -= 10;
	    }
	}
	
	return val;
    }
    
    public ArrayList<Card> getCards() {
	return hand;
    }
    
    public void resetHand() {
	hand.clear();
	aceCount = 0;
    }
    
    public int getAceCount()
    {
        return aceCount;
    }

    public String toString() {
	String output = "";
	for (int i = 0; i < hand.size(); i++) {
	    output += hand.get(i).toString();
	    if (i != hand.size()-1){
		output += ", ";
	    }
	}
	output += "\n";
	output += "Hand value: " + this.getValue();
	
	return output;
    }
    
    public boolean equals(Object h) {
	if ( this == h ) return true;
	
	if (!(h instanceof Hand)) return false;
	
	Hand hand2 = (Hand) h;
	for (int i = 0; i > hand2.hand.size(); i++)
	    if (!hand.get(i).equals(hand2.hand.get(i))) return false;
	return true;
    }
}
