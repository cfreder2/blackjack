package blackjack;


public class Dealer implements Player
{
    private Hand playerHand;
    private Hand dealerHand;
    private Deck deck;
    
    public Dealer(Hand ph, Hand dh, Deck d)
    {
	playerHand = ph;
	dealerHand = dh;
	deck = d;
    }

    @Override
    public GameStatus decide(Dealer dealer)
    {
	System.out.println("\nDealer's turn.");
	System.out.println(this.toString());
	if (dealerHand.getValue() < 17) {
	    hit();
	}
	else 
	{
	    stand();
	    return GameStatus.GAMEENDED;
	    
	}
	return GameStatus.DEALERTURN;
    }
    
    private void hit() {
	dealCard(GameStatus.DEALERTURN);
	System.out.println("Dealer hits.");
    }
    
    private void stand() {
	System.out.println("Dealer stands.");
    }
    
    public void shuffle(Deck deck) {
	deck.shuffle();
    }
    
    public void resetGame() {
	playerHand.resetHand();
	dealerHand.resetHand();
	deck.initializeDeck();
    }

    public void dealHand() {

	playerHand.drawCard(deck.drawCard());
	playerHand.drawCard(deck.drawCard());
	dealerHand.drawCard(deck.drawCard());
	dealerHand.drawCard(deck.drawCard());	
    }
    
    public void dealCard(GameStatus status) {
	if (status == GameStatus.PLAYERTURN) {
	    playerHand.drawCard(deck.drawCard());
	}
	else if (status == GameStatus.DEALERTURN) {
	    dealerHand.drawCard(deck.drawCard());
	}

    }
    
    public void shuffle() {
	deck.shuffle();
    }
    
    public Hand getPlayerHand() {
	return new Hand(playerHand.getCards(), playerHand.getAceCount()); //don't even think about it, player...
    }
    
    public Hand getDealerHand() {
	return new Hand(dealerHand.getCards(), dealerHand.getAceCount());
    }
    
    public String toString() {
	return "Dealer's hand: " + dealerHand.toString();
    }
}
