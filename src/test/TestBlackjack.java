package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;

import blackjack.*;
import blackjack.Card.Suit;
import blackjack.Card.Value;

public class TestBlackjack
{
    Dealer dealer;
    Human human;
    Hand hand1;
    Hand hand2;
    Deck deck1;
    Deck deck2;
    Card ace1;
    Card ace2;
    Card ten;
    Card two;
    Card nine;
    GameService service;
    GameStatus status;
    

    
	@Before
	public void setUp() 
	{
		human = new Human();
		hand1 = new Hand();
		hand2 = new Hand();
		deck1 = new Deck();
		deck2 = new Deck();
		ace1 = new Card(Value.ACE, Suit.SPADES);
		ace2 = new Card(Value.ACE, Suit.SPADES);
		ten = new Card(Value.TEN, Suit.HEARTS);
		two =  new Card(Value.TWO, Suit.CLUBS);
		nine = new Card(Value.NINE, Suit.CLUBS);
		dealer = new Dealer(hand1, hand2, deck1);
		service = new GameService();
		status = GameStatus.PLAYERFINISHED;
		

	}
	
	@After
	public void tearDown()
	{
	    dealer = null;
	    human = null;
	    hand1 = null;
	    hand2 = null;
	    service = null;
	}
	
	@Test
	public void testGetValue()
	{
	    assertEquals(hand1.getValue(), 0);
	    hand1.drawCard(ace1);
	    assertNotEquals(hand1.getValue(), hand2.getValue());
	    hand2.drawCard(ace2);
	    assertEquals(hand1.getValue(), 11);
	    assertEquals(hand1.getValue(), hand2.getValue());
	    hand1.drawCard(ace1);
	    assertEquals(hand1.getValue(), 12);
	    hand2.drawCard(two);
	    assertEquals(hand2.getValue(), 13);
	}
	
	@Test
	public void testInitializeDeck() //constructor initializes deck
	{
	    assertEquals("incorrect deck size", 52, deck1.getDeck().size());
	    for (int i = 0; i < 52; i++) {
		assertEquals("Unequal card on deck init", deck1.getDeck().get(i), deck2.getDeck().get(i));
	    }
	}
	
	@Test
	public void testShuffle()
	{
	    deck2.shuffle();
	    assertNotEquals("Decks are equal", deck1.getDeck(), deck2.getDeck());
	    
	}
	
	@Test
	public void testResetHandAndResetGame()
	{
	    dealer.dealHand();
	    hand1.resetHand();
	    hand2.resetHand();
	    assertEquals(0, hand1.getCards().size());
	    assertEquals(0, hand2.getCards().size());
	    dealer.resetGame();
	    assertEquals(0, hand1.getCards().size());
	    assertEquals(0, hand2.getCards().size());
	}
	
	@Test
	public void testCheckInitialGameStatus() {
	    hand1.drawCard(ace1);
	    hand1.drawCard(ten);
	    assertEquals(GameStatus.PLAYERWON, service.checkInitialGameStatus(dealer));

	    hand2.drawCard(ace1);
	    hand2.drawCard(ten);
	    assertEquals(GameStatus.TIE, service.checkInitialGameStatus(dealer));
	    
	    hand1.resetHand();
	    assertEquals(GameStatus.DEALERWON, service.checkInitialGameStatus(dealer));
	    hand2.resetHand();
	    assertEquals(GameStatus.PLAYERTURN, service.checkInitialGameStatus(dealer));
	}
	
	@Test
	public void testCheckGameStatus() {
	    hand1.drawCard(ten);
	    hand1.drawCard(ten);
	    assertEquals(GameStatus.DEALERTURN, service.checkGameStatus(status, dealer));
	    
	    status = GameStatus.PLAYERTURN;
	    assertEquals(GameStatus.PLAYERTURN, service.checkGameStatus(status, dealer));
	    status = GameStatus.PLAYERFINISHED;
	    
	    hand1.resetHand();
	    hand1.drawCard(ten);
	    hand1.drawCard(ten);
	    hand1.drawCard(two);
	    hand1.drawCard(ten);
	    hand1.drawCard(ten);
	    assertEquals(GameStatus.DEALERWON, service.checkGameStatus(status, dealer));
	    
	    hand1.resetHand();

	    hand1.drawCard(ten);
	    hand1.drawCard(ten);
	    hand2.drawCard(ten);
	    hand2.drawCard(two);

	    status = GameStatus.DEALERTURN;
	    assertEquals(GameStatus.DEALERTURN, service.checkGameStatus(status, dealer));
	    
	    hand1.resetHand();
	    hand2.resetHand();

	    hand1.drawCard(ten);
	    hand1.drawCard(ten);
	    hand2.drawCard(ten);
	    hand2.drawCard(two);
	    hand2.drawCard(ten);
	    status = GameStatus.GAMEENDED;
	    assertEquals(GameStatus.PLAYERWON, service.checkGameStatus(status, dealer));
	    
	    hand1.resetHand();
	    hand2.resetHand();
	    
	    hand1.drawCard(ten);
	    hand1.drawCard(ten);
	    hand2.drawCard(ten);
	    hand2.drawCard(nine);
	    status = GameStatus.GAMEENDED;
	    assertEquals(GameStatus.PLAYERWON, service.checkGameStatus(status, dealer));
	    
	    hand1.resetHand();
	    hand2.resetHand();
	    
	    hand1.drawCard(ten);
	    hand1.drawCard(ten);
	    hand1.drawCard(ace1);
	    hand2.drawCard(ten);
	    hand2.drawCard(two);
	    hand2.drawCard(nine);

	    status = GameStatus.GAMEENDED;
	    assertEquals(GameStatus.TIE, service.checkGameStatus(status, dealer));
	    
	    hand1.resetHand();
	    hand2.resetHand();
	    
	    hand1.drawCard(ten);
	    hand1.drawCard(ten);
	    hand2.drawCard(ten);
	    hand2.drawCard(two);
	    hand2.drawCard(nine);

	    status = GameStatus.GAMEENDED;
	    assertEquals(GameStatus.DEALERWON, service.checkGameStatus(status, dealer));
	}
}